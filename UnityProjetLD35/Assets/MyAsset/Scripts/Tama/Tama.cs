﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Tama : MonoBehaviour {


    CharInterractManager soundManager;

    float time = 0;
    float stateTime = 0;
    TamaStates currentState;
    TamaStates nextState;

    float sleepBar = 100;
    float hungryBar = 100;
    float cleanBar = 100;
    float happyBar = 100;

    public float moveSpeed = 5;
    Vector3 target;
    bool haveTarget = false;

    public float sleepDecRate;
    public float hungryDecRate;
    public float cleanDecRate;
    public float happyDecRate;

    public GameObject hungerIcon;
    public GameObject sleepIcon;
    public GameObject cleanIcon;


    float hungerLimit = 45f;
    float sleepLimit = 45f;
    float cleanLimit = 45f;
    float healthLimit = 45f;

    Animator ator;

    // Use this for initialization
    void Start ()
    {
        soundManager = GameObject.FindObjectOfType<CharInterractManager>();
        ator =  GetComponent<Animator>();
        currentState = TamaStates.Idle;
    }
	
	// Update is called once per frame
	void Update ()
    {
        HandleEnvy();
        UpdateState();
        GetNextState();

        if(haveTarget && currentState == TamaStates.Idle)
            GoToTarget();

        if (currentState == TamaStates.Idle && nextState == TamaStates.Adventuring)
            currentState = TamaStates.Adventuring;

        if (currentState == TamaStates.Idle && nextState == TamaStates.Dead)
            currentState = TamaStates.Dead;
    }

    void GetNextState()
    {

    }

    void returnToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    void UpdateState()
    {
        
        switch (currentState)
        {
            case TamaStates.Pooping:
            {
                sleepBar -= Time.deltaTime * sleepDecRate/4;
                hungryBar -= Time.deltaTime * hungryDecRate/4;
                if (time == 0)
                {
                    GetComponent<SpriteRenderer>().enabled = false;
                    GameObject.FindGameObjectWithTag("Douche").GetComponent<ObjectController>().Switch();
                    soundManager.playCharEvent(CharInterractManager.CharEventEnum.Shower);
                    stateTime = 7.9f;
                }

                time += Time.deltaTime;

                if (time > stateTime)
                {
                    GetComponent<SpriteRenderer>().enabled = true;
                    GameObject.FindGameObjectWithTag("Douche").GetComponent<ObjectController>().Switch();
                    stateTime = 0f;
                    time = 0f;

                    cleanBar += 70f;
                    currentState = TamaStates.Idle;
                }

            }
            break;

            case TamaStates.Idle:
            {
                DecreaseValues();

                if (!haveTarget)
                    if (Random.Range(0, 100) > 98.5f)
                    {
                        float min = Camera.main.GetComponent<CameraDrag>().minPos + 1;
                        float max = Camera.main.GetComponent<CameraDrag>().maxPos + 1;
                        float posX = Random.Range(min, max);
                        SetTarget(new Vector3(posX, 0, 0));
                    }
            }
            break;

            case TamaStates.Sleeping:
            {
                hungryBar -= Time.deltaTime * hungryDecRate / 4;
                cleanBar -= Time.deltaTime * cleanDecRate / 4;
                if (time == 0)
                {
                    GetComponent<SpriteRenderer>().enabled = false;
                    GameObject.FindGameObjectWithTag("Lit").GetComponent<ObjectController>().Switch();
                    soundManager.playCharEvent(CharInterractManager.CharEventEnum.Dodo);
                    stateTime = 8f;
                }

                time += Time.deltaTime;

                if (time > stateTime)
                {
                    GetComponent<SpriteRenderer>().enabled = true;
                    GameObject.FindGameObjectWithTag("Lit").GetComponent<ObjectController>().Switch();
                    stateTime = 0f;
                    time = 0f;

                    sleepBar += 70f;
                    currentState = TamaStates.Idle;
                }

            }
            break;

            case TamaStates.Eating:
            {
                sleepBar -= Time.deltaTime * sleepDecRate / 4;
                cleanBar -= Time.deltaTime * cleanDecRate / 4;

                if (time == 0)
                {
                    soundManager.playCharEvent(CharInterractManager.CharEventEnum.Eat);
                    GetComponent<Animator>().SetBool("isEating", true);
                    stateTime = 4.8f;
                }

                time += Time.deltaTime;

                if (time > stateTime)
                {
                    GetComponent<Animator>().SetBool("isEating", false);
                    stateTime = 0f;
                    time = 0f;

                    hungryBar += 70f;
                    currentState = TamaStates.Idle;
                }

            }
            break;

            case TamaStates.Adventuring:
            {
                
                if (time == 0)
                {
                    haveTarget = false;
                    GetComponent<Animator>().SetBool("Moving", false);
                    GetComponent<Animator>().SetBool("isEating", false);
                    soundManager.playCharEvent(CharInterractManager.CharEventEnum.HeroTime);
					soundManager.charPresentInHouse = false;
                    GetComponent<Animator>().SetBool("IsHeroTime", true);
                    stateTime = 6f;

                    foreach(SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
                    {
                            sprite.enabled = false;
                    }

                        GetComponent<SpriteRenderer>().enabled = true;
                }

                time += Time.deltaTime;

                if (time > stateTime*Random.Range(2,8))
                {
                    GetComponent<Animator>().SetBool("IsHeroTime", false);
                    stateTime = 0f;
                    time = 0f;

                    sleepBar = 40f;
                    currentState = TamaStates.Idle;
                    nextState = TamaStates.Idle;

					soundManager.playCharEvent(CharInterractManager.CharEventEnum.HeroTimeReturn);
					soundManager.charPresentInHouse = true;

                    foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
                    {
                        sprite.enabled = true;
                    }
                 }
            }

            break;

            case TamaStates.Dead:
            {

                if (time == 0)
                {
                    haveTarget = false;
                    GetComponent<Animator>().SetBool("Moving", false);
                    GetComponent<Animator>().SetBool("isEating", false);
                    soundManager.playCharEvent(CharInterractManager.CharEventEnum.HeroTime);
					//soundManager.charPresentInHouse = false;
					soundManager.charAlive = false;
					GetComponent<Animator>().SetBool("IsHeroTime", true);
                    stateTime = 6f;

                    foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
                    {
                        sprite.enabled = false;
                    }

                    GetComponent<SpriteRenderer>().enabled = true;
                }

                time += Time.deltaTime;

                if (time > stateTime)
                {
                        this.gameObject.SetActive(false);

                        Camera.main.GetComponent<Animator>().SetBool("isDead", true);

                        Invoke("returnToMenu", 25f);
                        GameObject.FindObjectOfType<Rotator>().speed = 30;
                }
            }

            break;

            default:
                break;
        }
    }

    void GoToTarget()
    {
        if (target.x > transform.position.x)
            transform.position = new Vector3((transform.position.x + moveSpeed * Time.deltaTime),
                                             transform.position.y,
                                             transform.position.z);
        else if (target.x < transform.position.x)
            transform.position = new Vector3((transform.position.x - moveSpeed * Time.deltaTime),
                                 transform.position.y,
                                 transform.position.z);

        if ( Mathf.Abs(target.x - transform.position.x) < 0.1f )
        {
            haveTarget = false;
            ator.SetBool("Moving", false);
            currentState = nextState;
            nextState = TamaStates.Idle;
        }

        
    }

    void HandleEnvy()
    {
        //sleep bar
        if (sleepBar < sleepLimit && sleepIcon.activeInHierarchy == false)
        {
            sleepIcon.gameObject.SetActive(true);
            //play sound here
			soundManager.playCharEvent(CharInterractManager.CharEventEnum.Need);
        }
        else if (sleepBar > sleepLimit && sleepIcon.activeInHierarchy == true)
        {
            sleepIcon.gameObject.SetActive(false);
        }

        //hungry bar
		if (hungryBar < hungerLimit && hungerIcon.activeInHierarchy == false) 
		{
			hungerIcon.gameObject.SetActive (true);
			//play sound here
			soundManager.playCharEvent(CharInterractManager.CharEventEnum.Need);
		} 
		else if (hungryBar > hungerLimit && hungerIcon.activeInHierarchy == true) 
		{
			hungerIcon.gameObject.SetActive (false);
		}

        //clean bar
		if (cleanBar < cleanLimit && cleanIcon.activeInHierarchy == false) 
		{
			cleanIcon.gameObject.SetActive (true);
			//play sound here
			soundManager.playCharEvent(CharInterractManager.CharEventEnum.Need);
		} 
		else if (cleanBar > cleanLimit && cleanIcon.activeInHierarchy == true) 
		{
			cleanIcon.gameObject.SetActive (false);
		}
    }

    public void DecreaseValues()
    {
        sleepBar -= Time.deltaTime * sleepDecRate;
        hungryBar -= Time.deltaTime * hungryDecRate;
        cleanBar -= Time.deltaTime * cleanDecRate;
    }

    public void SetNextState(TamaStates state)
    {
        this.nextState = state;
    }

    public void SetTarget(Vector3 target, TamaStates nextState)
    {
        haveTarget = true;
        this.target = target;
        ator.SetBool("Moving", true);

        if (this.nextState == TamaStates.Adventuring)
            return;

        if (nextState == TamaStates.Idle)
        {
            this.nextState = nextState;
        }
        else
        {
            if (nextState == currentState)
                this.nextState = TamaStates.Idle;
            else
                this.nextState = nextState;
        }
        Debug.Log(this.nextState);
    }

    public void SetTarget(Vector3 target)
    {
        haveTarget = true;
        this.target = target;
        ator.SetBool("Moving", true);
        this.nextState = TamaStates.Idle;
    }

}
