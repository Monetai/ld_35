﻿using UnityEngine;
using System.Collections;

public class TamaState : MonoBehaviour {

    protected TamaStates state;
    protected Animator ator;
    protected Tama tama;

    public TamaState(Tama tama)
    {
        this.tama = tama;
    }

    void Start()
    {
        ator = tama.gameObject.GetComponent<Animator>();
    }
}
