﻿using UnityEngine;
using System.Collections;

public enum TamaStates
{
    Idle,
    Eating,
    Sick,
    Hungry,
    Sleeping,
    Pooping,
    Adventuring,
    Dead
}