﻿using UnityEngine;
using System.Collections;

public class WarningManager : MonoBehaviour {

    Tama tama;
    public Vector2 random1;
    public Vector2 random2;
    public AlphaAnim toActivate;

    float time = 0;
    float limitTime = 0;
    int nbPassage = 0;

    // Use this for initialization
    void Start ()
    {
        tama = GameObject.FindObjectOfType<Tama>();

        limitTime = Random.Range(random1.x, random1.y);
    }
	
	// Update is called once per frame
	void Update ()
    {
        time += Time.deltaTime;

        if(time > limitTime)
        {
            nbPassage++;
            limitTime = Random.Range(random2.x, random2.y);
            time = 0;

            if (nbPassage > 2)
            {
                if(Random.Range(0,10) < 8 )
                {
                    Debug.Log("IL EST MORT!!!");
                    limitTime = float.PositiveInfinity;
                    toActivate.GoToDeath();
                    PlayerPrefs.SetInt("Dead", 1);
                    PlayerPrefs.Save();
                }
                else
                    Debug.Log("OUF IL EST SAUF!!!");
            }

            toActivate.gameObject.SetActive(true);
        }
	}
}
