﻿using UnityEngine;
using System.Collections;

public class CameraDrag : MonoBehaviour
{
    public float minPos, maxPos;
    public float mouseSensitivity = 1.0f;
    private Vector3 lastPosition ;

    public float velocityDiviser = 4;

    private Vector3 _velocity;
    private bool _underInertia;
    private float _time = 0.0f;
    public float SmoothTime;

    Vector3 _curPosition;
    Vector3 _screenPoint;

    void Start()
    {
    }

    void Update()
    {
        MoveCamera();

        if (_underInertia && _time <= SmoothTime)
        {
            transform.position += new Vector3(_velocity.x/velocityDiviser,0,0);
            transform.position += new Vector3(_velocity.x,0,0);
            _velocity = Vector3.Lerp(_velocity, Vector3.zero, _time);
            _time += Time.smoothDeltaTime;
        }
        else
        {
            _underInertia = false;
            _time = 0.0f;
        }
    }

    public void MoveCamera()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _screenPoint = Camera.main.WorldToScreenPoint(Input.mousePosition);
            _underInertia = false;
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 _prevPosition = _curPosition;
            Vector3 _curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
            _curPosition = Camera.main.ScreenToWorldPoint(_curScreenPoint);

            var delta = Input.mousePosition - lastPosition;
            _velocity = _curPosition - _prevPosition;
            transform.Translate(-delta.x * mouseSensitivity,0, 0);
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            //_underInertia = true;
        }

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minPos, maxPos),
                                        transform.position.y,
                                        transform.position.z);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(new Vector3(transform.position.x + maxPos,0,0), 0.5f);
        Gizmos.DrawSphere(new Vector3(transform.position.x + minPos, 0, 0), 0.5f);

    }
}