﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuController : MonoBehaviour {

	MenuAudioManager menuSoundManager;
	CreditsAudioManager creditsSoundManager;

	// Use this for initialization
	void Start () {
		menuSoundManager = GameObject.FindObjectOfType<MenuAudioManager>();
		creditsSoundManager = GameObject.FindObjectOfType<CreditsAudioManager>();
	}

	public void Quit()
    {
		menuSoundManager.playCharEvent (MenuAudioManager.CharEventEnum.Clic);
        Application.Quit();
    }

    public void Play()
    {
		menuSoundManager.playCharEvent (MenuAudioManager.CharEventEnum.Clic);
        SceneManager.LoadScene("Inter");
    }

    public void Inter1()
    {
		menuSoundManager.playCharEvent (MenuAudioManager.CharEventEnum.Clic);
        SceneManager.LoadScene("EggChoice");
    }

    public void Inter2()
    {
		menuSoundManager.playCharEvent (MenuAudioManager.CharEventEnum.Clic);
		menuSoundManager.stopCharEvent (MenuAudioManager.CharEventEnum.MusicFox);
        SceneManager.LoadScene("MainScene");
    }

    public void Credit()
    {
		menuSoundManager.playCharEvent (MenuAudioManager.CharEventEnum.Clic);
        SceneManager.LoadScene("credits");
    }

    public void GoToMenu()
	{
		creditsSoundManager.playCharEvent (CreditsAudioManager.CharEventEnum.Clic);
        SceneManager.LoadScene("Menu");
    }

}
