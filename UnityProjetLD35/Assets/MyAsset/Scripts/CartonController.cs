﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CartonController : MonoBehaviour {

    public GameObject toActivate;
    public float activationTime;
    public string toLoad;
    float time = 0;
    bool activation = false;
    public bool chooseChar;

	CreditsAudioManager creditsSoundManager;

	// Use this for initialization
	void Start () {
		creditsSoundManager = GameObject.FindObjectOfType<CreditsAudioManager>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!activation)
        {
            time += Time.deltaTime;

            if (time > activationTime)
            {
                toActivate.SetActive(true);
                activation = true;
            }
        }
        else
        {
            if (Input.GetMouseButton(0))
			{
				creditsSoundManager.playCharEvent(CreditsAudioManager.CharEventEnum.Clic);

                if(chooseChar)
                {
                    int Chara = PlayerPrefs.GetInt("Char");

                    if (Chara == 0)
                    {
                        SceneManager.LoadScene("MainSceneTama");
                    }
                    else
                    {
                        SceneManager.LoadScene("MainSceneMout");
                    }
                }
                else
                    SceneManager.LoadScene(toLoad);
            }
                
        }

	}
}
