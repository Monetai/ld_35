﻿using UnityEngine;
using System.Collections;

public class ObjectUIHandler : MonoBehaviour {

    Tama tama;
    CharInterractManager soundManager;

    void Start()
    {
        tama = GameObject.FindObjectOfType<Tama>();
        soundManager = GameObject.FindObjectOfType<CharInterractManager>();
    }

    public void ShowerAction(Transform target)
    {
        soundManager.playCharEvent(CharInterractManager.CharEventEnum.Clic);
        tama.SetTarget(target.position, TamaStates.Pooping);
    }

    public void EatingAction(Transform target)
    {
        soundManager.playCharEvent(CharInterractManager.CharEventEnum.Clic);
        tama.SetTarget(target.position, TamaStates.Eating);
    }

    public void SleepAction(Transform target)
    {
        soundManager.playCharEvent(CharInterractManager.CharEventEnum.Clic);
        tama.SetTarget(target.position, TamaStates.Sleeping);
    }

}
