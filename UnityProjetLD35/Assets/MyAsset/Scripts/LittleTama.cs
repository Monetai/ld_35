﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LittleTama : MonoBehaviour {

    bool haveHatch = false;
    bool moveToCenter = false;
    bool isDrag = false;
    bool isOn = false;
    bool isSelected = false;

    public GameObject Egg;
    public GameObject Baby;

    public GameObject OtherEgg;

    public float moveSpeed = 2;

    float  happyBar = 0;
    public float happyInc =1;
    public string toLoad;


    public float time;
    public float hatchTime = 5;
    public int nbPlayerPref;

    Vector3 oldPos;

	SelectEggAudioManager soundManager;
	bool selectedsound = false;

    // Use this for initialization
	void Start () {
		soundManager = GameObject.FindObjectOfType<SelectEggAudioManager>();

        oldPos = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (moveToCenter)
            MoveToCenter();

        if (haveHatch)
        {
            Baby.GetComponent<Animator>().SetBool("isCudling", false);

            happyBar += Time.deltaTime * happyInc;

			soundManager.happynessBar = happyBar;

            if (happyBar >= 200)
                SceneManager.LoadScene(toLoad);

            if (isOn)
            {

                Vector3 newPos = Input.mousePosition;
                if (Vector3.Distance(newPos, oldPos) > 0.5)
                {
                    happyBar += Time.deltaTime * (happyInc*4);
                    Baby.GetComponent<Animator>().SetBool("isCudling", true);
				    soundManager.playCharEvent (SelectEggAudioManager.CharEventEnum.Caresse);
                }
                    
                oldPos = Input.mousePosition;

            }
       
        }
        else if(isSelected)
        {
			if (!selectedsound) {
				soundManager.playCharEvent (SelectEggAudioManager.CharEventEnum.Clic);
				selectedsound = true;
			}

            time += Time.deltaTime;
            if(time > hatchTime)
            {
				soundManager.playCharEvent (SelectEggAudioManager.CharEventEnum.OeufCrac);
                Egg.GetComponent<Animator>().SetBool("Hatch", true);
                Baby.SetActive(true);
                haveHatch = true;
            }
        }

    }

    void OnMouseUpAsButton()
    {
        if(!isSelected)
        {
            PlayerPrefs.SetInt("Char", nbPlayerPref);
            PlayerPrefs.Save();
            moveToCenter = true;
            OtherEgg.gameObject.SetActive(false);
        }

        isSelected = true;
    }

    void OnMouseEnter()
    {
        isOn = true;
    }

    void OnMouseDrag()
    {
        isDrag = true;
    }

    void OnMouseExit()
    {
        isOn = false;
    }

    void MoveToCenter()
    {
        if (0 > transform.position.x)
            transform.position = new Vector3((transform.position.x + moveSpeed * Time.deltaTime),
                                             transform.position.y,
                                             transform.position.z);
        else if (0 < transform.position.x)
            transform.position = new Vector3((transform.position.x - moveSpeed * Time.deltaTime),
                                 transform.position.y,
                                 transform.position.z);

        if (Mathf.Abs(0 - transform.position.x) < 0.1f)
        {
            moveToCenter = false;
        }
    }
}
