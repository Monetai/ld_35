﻿using UnityEngine;
using System.Collections;

public class SelectEggAudioManager : MonoBehaviour {

	public enum CharEventEnum
	{
		Caresse,
		OeufCrac,
		HeartBeat,
		MusicFox,
		Clic
	}

	FMODUnity.StudioEventEmitter eventHeartBeat;
	public float happynessBar;

	// Initialise les musics sur les bonnes variables
	void OnEnable () {
		
	}

	public void Update(){
		//eventHeartBeat.SetParameter ("Level", happynessBar);
	}

	private void playGoodSound(string str)
	{
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == str) {
				if (!fmodcomponent.IsPlaying ()) {
					fmodcomponent.Play ();
				}
			}
		}
	}

	public void playCharEvent(CharEventEnum charevent)
	{
		switch (charevent) {
		case CharEventEnum.Caresse:
			playGoodSound ("event:/PetitTruc/Caresse");
			break;
		case CharEventEnum.OeufCrac:
			playGoodSound ("event:/PetitTruc/OeufCrac");
			break;
		case CharEventEnum.Clic:
			playGoodSound ("event:/Ui/Clic");
			break;
		case CharEventEnum.HeartBeat:
			playGoodSound ("event:/PetitTruc/HeartBeat");
			break;
		}
	}
}
