﻿using UnityEngine;
using System.Collections;

public class ChangeMusicWhenGone : MonoBehaviour {

	FMODUnity.StudioEventEmitter eventMusicGame, eventMusicMenu;
	public bool charPresentInHouse;

	// Use this for initialization
	void OnEnable () {
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == "event:/Musics/MusicGame") {
				if (!fmodcomponent.IsPlaying ()) {
					eventMusicGame = fmodcomponent;
				}
			}
			if (fmodcomponent.Event == "event:/Musics/MusicMenu") {
				if (!fmodcomponent.IsPlaying ()) {
					eventMusicMenu = fmodcomponent;
				}
			}
		}

		charPresentInHouse = true;
	}

	// Update is called once per frame
	void Update () {
		if (charPresentInHouse==true) {
			eventMusicGame.SetParameter ("inOut", 0);
		} 
		if(charPresentInHouse==false){
			eventMusicGame.SetParameter ("inOut", 1);
		}
	}
}