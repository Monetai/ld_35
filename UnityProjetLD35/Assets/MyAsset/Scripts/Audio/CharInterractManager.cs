﻿using UnityEngine;
using System.Collections;

public class CharInterractManager : MonoBehaviour {

	public enum CharEventEnum
	{
		Eat,
		Shower,
		HeroTime,
		Dodo,
		Need,
		Warning,
		HeroTimeReturn,
		Clic,
		Coeur,
		OeufCrac
	}

	FMODUnity.StudioEventEmitter eventMusicGame, eventMusicMenu;
	public bool charPresentInHouse,charAlive;

	public void Start(){
		//GameObject.FindObjectOfType<MenuAudioManager>().changePlayMusicFox();
	}

	// Initialise les musics sur les bonnes variables
	void OnEnable () {
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == "event:/Musics/MusicGame") {
				if (!fmodcomponent.IsPlaying ()) {
					eventMusicGame = fmodcomponent;
				}
			}
			if (fmodcomponent.Event == "event:/Musics/MusicMenu") {
				if (!fmodcomponent.IsPlaying ()) {
					eventMusicMenu = fmodcomponent;
				}
			}
		}

		charPresentInHouse = true;
		charAlive = true;
	}

	public void Update(){
		if (Input.GetKey (KeyCode.W)) {
			playCharEvent (CharEventEnum.Shower);
		}
		if (Input.GetKey (KeyCode.X)) {
			playCharEvent (CharEventEnum.Eat);
		}
		if (Input.GetKey (KeyCode.C)) {
			playCharEvent (CharEventEnum.HeroTime);
		}
		if (Input.GetKey (KeyCode.V)) {
			playCharEvent (CharEventEnum.Clic);
		}
		if (Input.GetKey (KeyCode.B)) {
			playCharEvent (CharEventEnum.Dodo);
		}
		if (Input.GetKey (KeyCode.N)) {
			playCharEvent (CharEventEnum.Warning);
		}
			
		// Gerer la musique quand le hero est parti
		if (charPresentInHouse==true) {
			eventMusicGame.SetParameter ("inOut", 0);
		} 
		if(charPresentInHouse==false){
			eventMusicGame.SetParameter ("inOut", 1);
		}
		if(charAlive==true){
			eventMusicGame.SetParameter ("ending", 0);
		}
		if(charAlive==false){
			eventMusicGame.SetParameter ("ending", 1);
		}

	}

	private void playGoodSound(string str)
	{
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == str) {
				if (!fmodcomponent.IsPlaying ()) {
					fmodcomponent.Play ();
				}
			}
		}
	}

	public void playCharEvent(CharEventEnum charevent)
	{
		switch (charevent) {
			case CharEventEnum.Shower:
				playGoodSound ("event:/Character/Shower");
				break;
			case CharEventEnum.Eat:
				playGoodSound ("event:/Character/Eat");
				break;
			case CharEventEnum.HeroTime:
				playGoodSound ("event:/Character/HeroTime");
				break;
			case CharEventEnum.Clic:
				playGoodSound ("event:/Ui/Clic");
				break;
			case CharEventEnum.Dodo:
				playGoodSound ("event:/Character/Dodo");
				break;
			case CharEventEnum.Warning:
				playGoodSound ("event:/Ui/Warning");
				break;
			case CharEventEnum.Coeur:
				playGoodSound ("event:/PetitTruc/Coeur");
				break;
			case CharEventEnum.OeufCrac:
				playGoodSound ("event:/PetitTruc/OeufCrac");
				break;
			case CharEventEnum.HeroTimeReturn:
				playGoodSound ("event:/Character/HeroTimeReturn");
				break;
			case CharEventEnum.Need:
				playGoodSound ("event:/Character/Need");
				break;
		}
	}
}
