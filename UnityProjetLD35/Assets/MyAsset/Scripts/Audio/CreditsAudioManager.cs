﻿using UnityEngine;
using System.Collections;

public class CreditsAudioManager : MonoBehaviour {

	public enum CharEventEnum
	{
		Clic
	}
		
	private void playGoodSound(string str)
	{
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == str) {
				if (!fmodcomponent.IsPlaying ()) {
					fmodcomponent.Play ();
				}
			}
		}
	}

	public void playCharEvent(CharEventEnum charevent)
	{
		switch (charevent) {
		case CharEventEnum.Clic:
			playGoodSound ("event:/Ui/Clic");
			break;
		}
	}
}
