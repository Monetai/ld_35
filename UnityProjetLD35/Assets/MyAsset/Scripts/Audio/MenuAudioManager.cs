﻿using UnityEngine;
using System.Collections;

public class MenuAudioManager : MonoBehaviour {

	public enum CharEventEnum
	{
		Clic,
		MusicFox
	}

	static public bool beginGame = false;
	static public bool playMusicFox = true;

	public void Start(){
		if (beginGame == false) {
			
			foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
				if (fmodcomponent.Event == "event:/Musics/MusicFox") {
					if (!fmodcomponent.IsPlaying ()) {
						//fmodcomponent.Play ();
					}
				}
			}
			beginGame = true;
		}
	}

	public void Update(){
		if (playMusicFox == false) {
			foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
				if (fmodcomponent.Event == "event:/Musics/MusicFox") {
					if (!fmodcomponent.IsPlaying ()) {
						fmodcomponent.Stop ();
					}
				}
			}
		}
	}

	private void playGoodSound(string str)
	{
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == str) {
				if (!fmodcomponent.IsPlaying ()) {
					fmodcomponent.Play ();
				}
			}
		}
	}

	public void changePlayMusicFox(){
		playMusicFox = false;
	}

	private void stopGoodSound(string str)
	{
		foreach (FMODUnity.StudioEventEmitter fmodcomponent in GetComponents<FMODUnity.StudioEventEmitter> ()) {
			if (fmodcomponent.Event == str) {
				fmodcomponent.Stop ();
			}
		}
	}

	public void playCharEvent(CharEventEnum charevent)
	{
		switch (charevent) {
		case CharEventEnum.Clic:
			playGoodSound ("event:/Ui/Clic");
			break;
		case CharEventEnum.MusicFox:
			playGoodSound ("event:/Musics/MusicFox");
			break;
		}
	}

	public void stopCharEvent(CharEventEnum charevent)
	{
		switch (charevent) {
		case CharEventEnum.MusicFox:
			playGoodSound ("event:/Musics/MusicFox");
			break;
		}
	}
}