﻿using UnityEngine;
using System.Collections;

public class AlphaAnim : MonoBehaviour {

    Tama tama;

    CharInterractManager soundManager;
    SpriteRenderer[] rends;
    public float duration = 1.0f;
    float alpha = 0;
    public bool animate = false;
    public float maxDuration;
    float time;

    TamaStates toAdventure = TamaStates.Adventuring;

    // Use this for initialization
    void Start ()
    {
        tama = GameObject.FindObjectOfType<Tama>();
        rends = GetComponentsInChildren<SpriteRenderer>();
        soundManager = GameObject.FindObjectOfType<CharInterractManager>();
    }

    public void Anim()
    {
        soundManager.playCharEvent(CharInterractManager.CharEventEnum.Warning);
        animate = true;
    }

    void GoToAdventure()
    {
        tama.SetNextState(toAdventure);
    }

    public void GoToDeath()
    {
        toAdventure = TamaStates.Dead;
    }

    public void Stop()
    {
        animate = false;
    }

    void Update()
    {
        if (animate)
        {
            lerpAlpha();
            time += Time.deltaTime;

            if(time>maxDuration && alpha < 0.05)
            {
                time = 0;
                alpha = 0;
                GoToAdventure();
                gameObject.SetActive(false);
            }
        }
    }

    void lerpAlpha()
    {
        float lerp  = Mathf.PingPong(Time.time, duration) / duration;

        alpha = Mathf.Lerp(0.0f, 1.0f, lerp);
        foreach(SpriteRenderer rend in rends)
        {
            rend.color = new Color (rend.color.r, rend.color.g, rend.color.b,alpha);
        }
        
    }
}
