﻿using UnityEngine;
using System.Collections;

public class ObjectController : MonoBehaviour {

    Tama tama;
    Animator ator;
    bool menuVisible = false;
    public float visibleTime = 0.5f;
    float time = 0f;

    public Sprite secondState;
    Sprite mainState;
    bool isMainState = true;
    CharInterractManager soundManager;

    // Use this for initialization
    void Start ()
    {
        tama = GameObject.FindObjectOfType<Tama>();
        ator = transform.GetComponentInChildren<Animator>();
        mainState = GetComponent<SpriteRenderer>().sprite;
        soundManager = GameObject.FindObjectOfType<CharInterractManager>();

    }

    void Update()
    {
        if (menuVisible)
            time += Time.deltaTime;
        if (time > visibleTime)
        {
            ator.SetBool("MenuActive", false);
            time = 0;
            menuVisible = false;
        }
    }

    void OnMouseUpAsButton()
    {
        //soundManager.playCharEvent(CharInterractManager.CharEventEnum.Clic);
        //menuVisible = true;
        
        //Ouverture du menu
    }

    void OnMouseEnter()
    {
        menuVisible = true;
        ator.SetBool("MenuActive", true);
        //ator.SetBool("OnObject", false);
        //ator.SetBool("OnObject", true);
        //Mise en valeur de l'objet
    }

    void OnMouseOver()
    {
        menuVisible = true;
        time = 0;
    }

    void OnMouseExit()
    {
        ator.SetBool("OnObject", false);
        //Retour de l'objet a l'etat normal
    }

    public void Switch()
    {
        if (isMainState)
        {
            GetComponent<SpriteRenderer>().sprite = secondState;
            isMainState = false;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = mainState;
            isMainState = true;
        }
    }
}
