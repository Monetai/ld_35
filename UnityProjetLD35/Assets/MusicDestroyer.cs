﻿using UnityEngine;
using System.Collections;

public class MusicDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(GameObject.FindObjectOfType<tempMusic> () != null)
			GameObject.FindObjectOfType<tempMusic> ().Destroy ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
