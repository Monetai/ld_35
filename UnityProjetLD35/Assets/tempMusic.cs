﻿using UnityEngine;
using FMODUnity;
using System.Collections;

public class tempMusic : MonoBehaviour {

	bool isFirst = false;
	// Use this for initialization
	void Start () {

		int nbSound = GameObject.FindObjectsOfType<tempMusic> ().Length;
		Debug.Log (nbSound);
		if (nbSound == 1) 
		{
			DontDestroyOnLoad (this.gameObject);
			isFirst = true;
			GetComponent<FMODUnity.StudioEventEmitter> ().Play ();
		} else if (!isFirst) 
		{
			Destroy (this.gameObject);

		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Destroy()
	{
		GetComponent<FMODUnity.StudioEventEmitter> ().Stop ();
	}
}
