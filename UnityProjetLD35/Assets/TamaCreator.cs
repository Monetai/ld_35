﻿using UnityEngine;
using System.Collections;

public class TamaCreator : MonoBehaviour {

    public GameObject Tama;
    public GameObject Mout;

    // Use this for initialization
    void Awake () {
        int Chara  = PlayerPrefs.GetInt("Char");

        if (Chara == 0 )
        {
            Destroy(Mout);
        }
        else
        {
            Destroy(Tama);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
